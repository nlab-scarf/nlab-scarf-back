from .settings import *

if DEBUG:
    try:
        from .local_settings import *
    except ImportError:
        pass
