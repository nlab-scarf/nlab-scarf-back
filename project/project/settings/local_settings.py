from project.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'nlab_scarf',
        'USER': 'nlab_back',
        'PASSWORD': 'nlab_back_pass',
        'HOST': '0.0.0.0',
        'PORT': 6431
    }
}

BROKER_URL = 'amqp://rabbitmq_nlab:rabbitmq_nlab@0.0.0.0:5672//'
CELERY_BROKER_URL = 'amqp://rabbitmq_nlab:rabbitmq_nlab@0.0.0.0:5672//'
