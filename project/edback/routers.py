from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from rest_framework_nested.routers import NestedSimpleRouter

from .views import *


router = DefaultRouter()
router.register(r'learning', LearningViewSet)
router.register(r'environment', EnvironmentViewSet)

env_params_router = NestedSimpleRouter(router, r'environment', lookup='environment')
env_params_router.register(r'params', EnvParamsViewSet, base_name='environment-params')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(env_params_router.urls))
]
