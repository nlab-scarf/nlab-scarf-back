import logging
import re
from time import sleep

from edback.docker_utils import pull_image, create_container, create_network
from edback.json_rpc import JSONRPCClient
from edback.models import Environment, Learning
from project.celery import app


@app.task(bind=True)
def debug_task(self, pk):
    for i in range(5):
        sleep(i)
        print(f'Sleep {i}')
    print('Request: {0!r}'.format(self.request))
    return 1


@app.task(bind=True)
def pull_image_task(self, pk):
    env = Environment.objects.get(pk=pk)
    env.state = Environment.StateChoice.BUILDING
    env.save()
    try:
        pull_image(env.registry_url)
    except Exception as ex:
        env.state = Environment.StateChoice.ERROR
        env.message = repr(ex)[:1024]
    else:
        env.state = Environment.StateChoice.READY
        env.message = ""
    finally:
        env.save()


@app.task(bind=True)
def create_learning(self, pk):
    lrn = Learning.objects.get(pk=pk)
    lrn.network = f'scarf_net_{pk}'
    create_network(lrn.network)
    command = lrn.get_nlab_command()
    lrn.deploy_address = 13550+int(pk)
    trans_text = re.sub(r'(?u)[^-\w]', '', lrn.nlab.name)
    cont_name = f'{trans_text}_{pk}'
    create_container(lrn.nlab.registry_url, lrn.network, cont_name,
                     command, ports={'13550/tcp': int(lrn.deploy_address)})

    JSONRPCClient(host_port=lrn.deploy_address).send_request(
        'start', **{"rounds": lrn.rounds, "popsize": lrn.pop_size})

    command = lrn.get_env_command(cont_name)
    trans_text = re.sub(r'(?u)[^-\w]', '', lrn.environment.name)
    create_container(lrn.environment.registry_url, lrn.network, f'env-{trans_text}_{pk}', command)
    lrn.save()


@app.task(bind=True)
def delete_learning(self, pk):
    pass
