from tinyrpc.protocols.jsonrpc import JSONRPCProtocol

from edback.tcp_stream import TcpStream


class JSONRPCClient:
    def __init__(self, host_ip='0.0.0.0', host_port=13550):
        self.stream = TcpStream(host_ip, host_port, 1000)
        self.stream.connect()
        self.rpc = JSONRPCProtocol()

    def send_request(self, method_name, **kwargs):
        request = self.rpc.create_request(method_name, kwargs=kwargs)

        req_bytes = bytes(request.serialize() + "\0", encoding='utf8')

        self.stream.send(req_bytes)
        rep_bytes = self.stream.receive()

        reply = self.rpc.parse_reply(rep_bytes.decode().strip("\0 "))
        self.stream.disconnect()
        return reply
