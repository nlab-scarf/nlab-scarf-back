from django.test import TestCase

from django.urls import reverse
# Create your tests here.
from .models import Environment


class EnvironmentTestCase(TestCase):
    def test_creation(self):
        resp = self.client.post(reverse('environment-list'), content_type='application/json',
                                data={'name': 'qwer',
                                      'repo_url': 'we',
                                      'command': 'python main.py',
                                      'registry_url': 'shehtmoney/neurolab',
                                      'params': [{'key': '--no-gui',
                                                  'description': 'run without gui'},
                                                 {'key': '--no-gui',
                                                  'description': 'run without gui'}]})
        print(resp.json())
        self.assertEqual(resp.status_code, 201)

    def test_change_only_env(self):
        self.test_creation()
        resp = self.client.patch(reverse('environment-detail', args=(1, )), content_type='application/json',
                                 data={'name': 'qwer',
                                       'repo_url': 'we',
                                       'registry_url': 'shehtmoney/neurolab',
                                       'state': 1,
                                       'params': [{'key': '--no-gui',
                                                   'description': 'run without gui'},
                                                  {'key': '--no-gui',
                                                   'description': 'run without gui'}]})
        print(resp.json())
        self.assertEqual(resp.status_code, 200)

    def test_delete_current_param(self):
        self.test_creation()
        prev_len = len(Environment.objects.get(pk=1).param_set.all())
        print(prev_len)
        resp = self.client.delete(reverse('param-detail', args=(1, 1)))
        new_len = len(Environment.objects.get(pk=1).param_set.all())
        self.assertEqual(resp.status_code, 204)
        self.assertEqual(prev_len, new_len + 1)

    def test_build_environment(self):
        self.test_creation()
        resp = self.client.get(reverse('environment-build', args=(1, )))
        state = Environment.objects.get(pk=1).state
        print(Environment.objects.get(pk=1).state)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(state, Environment.StateChoice.READY)


class LearningTestCase(TestCase):
    pass
