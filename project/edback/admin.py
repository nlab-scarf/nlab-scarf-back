from django.contrib import admin

from .models import Environment, NeuroServer, Learning


@admin.register(Environment)
class EnvironmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Learning)
class LearningAdmin(admin.ModelAdmin):
    pass


@admin.register(NeuroServer)
class NLabAdmin(admin.ModelAdmin):
    pass
