import docker


def pull_image(url):
    client = docker.from_env()
    client.images.pull(url)


def create_container(image, net_name, name, command, ports=None):
    client = docker.from_env()
    cont = client.containers.create(image=image, network=net_name,
                                    cpus='0.5', name=name,
                                    ports=ports, command=command)
    cont.run()


def create_network(name):
    client = docker.from_env()
    net = client.networks.create(name)
