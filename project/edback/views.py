from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from edback.json_rpc import JSONRPCClient
from edback.tasks import debug_task, pull_image_task, create_learning
from .models import Learning, Environment, Param
from .serializers import LearningSerializer, EnvSerializer, EnvParamSerializer


class LearningViewSet(ModelViewSet):
    """
    API endpoint that allows to view or edit leanings.
    """
    queryset = Learning.objects.all()
    serializer_class = LearningSerializer

    @action(detail=True, methods=['GET'], url_path='build')
    def build(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        qwe = create_learning(pk)
        serializer = self.get_serializer(learn)
        return Response(serializer.data, status=200)

    @action(detail=True, methods=['GET'], url_path='stop')
    def stop(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('stop')
        return Response(resp, status=200)
    
    @action(detail=True, methods=['GET'], url_path='start')
    def start(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('start')
        return Response(resp, status=200)

    @action(detail=True, methods=['GET'], url_path='pause')
    def pause(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('pause')
        return Response(resp, status=200)

    @action(detail=True, methods=['GET'], url_path='pause')
    def pause(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('pause')
        return Response(resp, status=200)

    @action(detail=True, methods=['GET'], url_path='get_population')
    def get_population(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('get_population')
        return Response(resp, status=200)

    @action(detail=True, methods=['GET'], url_path='get_state')
    def get_state(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Learning.objects.filter(id=pk)
        learn = get_object_or_404(qs)
        resp = JSONRPCClient(host_port=learn.deploy_address).send_request('get_state')
        return Response(resp, status=200)


class EnvironmentViewSet(ModelViewSet):
    """
    API endpoint that allows view or edit envs.
    """
    queryset = Environment.objects.all()
    serializer_class = EnvSerializer

    @action(detail=False, methods=['GET'], url_path='all')
    def all(self, request, *args, **kwargs):
        self.queryset = Environment.objects.filter(state=Environment.StateChoice.READY)
        context = self.get_serializer_context()
        serializer = self.get_serializer(self.queryset, many=True, context=context)
        return Response(serializer.data)

    @action(detail=True, methods=['GET'], url_path='build')
    def build(self, request, *args, **kwargs):
        pk = kwargs['pk']
        qs = Environment.objects.filter(id=pk)
        env = get_object_or_404(qs)
        if env.state == env.state == Environment.StateChoice.BUILDING:
            return Response(status=403)
        qwe = pull_image_task.delay(pk)
        serializer = self.get_serializer(env)
        return Response(serializer.data, status=200)


class EnvParamsViewSet(ModelViewSet):
    queryset = Param.objects.all()
    serializer_class = EnvParamSerializer

    def get_queryset(self):
        return Param.objects.filter(environment_id=int(self.kwargs['environment_pk']))

    def perform_create(self, serializer):
        serializer.save(environment=Environment.objects.get(id=int(self.kwargs['environment_pk'])))

