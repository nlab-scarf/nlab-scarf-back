from rest_framework import serializers

from .models import Learning, Environment, Param


class EnvParamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Param
        fields = ('id', 'key', 'description', 'required', 'type')


class LearningSerializer(serializers.ModelSerializer):
    nlab_name = serializers.ReadOnlyField(source='nlab.name')
    env_name = serializers.ReadOnlyField(source='environment.name')

    class Meta:
        model = Learning
        fields = ('id', 'name', 'learn_params', 'environment', 'nlab_name', 'env_name',
                  'nlab', 'rounds', 'pop_size', 'network', 'created_at', 'deploy_address')
        read_only = ('network', 'created_at', 'deploy_address')


class EnvSerializer(serializers.ModelSerializer):
    params = EnvParamSerializer(required=False, many=True, source='param_set')

    def create(self, validated_data):
        params_data = validated_data.pop('param_set', [])
        environment = Environment.objects.create(**validated_data)
        for param in params_data:
            Param.objects.create(environment=environment, **param)
        return environment

    def update(self, instance, validated_data):
        params_data = validated_data.pop('param_set', [])
        instance = super().update(instance, validated_data)
        for param in params_data:
            par_id = param.pop('id', None)
            if not par_id:
                continue
            qs = Param.objects.filter(environment=instance.id, pk=par_id).update(**param)
            print(qs.query)
        return instance

    class Meta:
        model = Environment
        fields = ('id', 'name', 'command', 'repo_url', 'registry_url', 'params', 'state', 'message', 'created_at')
        read_only = ('state', 'message', 'created_at')
