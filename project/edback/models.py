import re
from datetime import datetime

from django.contrib.postgres import fields
from django.db import models


class Param(models.Model):
    class ParamChoice:
        PARAM = 0
        FLAG = 1
        ARG = 2

        CHOICES = (
            (PARAM, 'key-value param'),
            (FLAG, 'boolean flag'),
            (ARG, 'argument'),
        )

    key = models.CharField(max_length=512)
    description = models.CharField(max_length=1024, null=True, blank=True)
    required = models.BooleanField(default=False)
    type = models.SmallIntegerField(choices=ParamChoice.CHOICES, default=ParamChoice.PARAM)

    # possible_values = fields.ArrayField(models.CharField(max_length=255), null=True, blank=True)

    environment = models.ForeignKey('Environment', null=True, blank=True, on_delete=models.CASCADE)
    nlab = models.ForeignKey('NeuroServer', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.key + (self.environment if self.environment.name else '') + (self.nlab.name if self.nlab else '')


class Learning(models.Model):
    created_at = models.DateTimeField(default=datetime.utcnow)
    pub_date = models.DateTimeField('date published', null=True)
    name = models.CharField(max_length=255, null=True)

    learn_params = models.ManyToManyField('Param', through='LearningParamValue')

    environment = models.ForeignKey('Environment', null=True, blank=True, on_delete=models.SET_NULL)
    nlab = models.ForeignKey('NeuroServer', null=True, blank=True, on_delete=models.SET_NULL)

    network = models.CharField(max_length=1024, null=True, blank=True)

    deploy_address = models.IntegerField(null=True, blank=True)
    rounds = models.IntegerField(default=10)
    pop_size = models.IntegerField(default=25)

    def get_env_command(self, cont_name):
        params = self.learn_params.filter(environment__isnull=False)
        com = self._get_command(params)
        if '--uri' in com:
            re.sub(r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.)'
                     r'{3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])', cont_name, com)
        else:
            com = f'--uri tcp://{cont_name}:5005' + com
        return f'{self.nlab.command} {com}'

    def get_nlab_command(self):
        params = self.learn_params.filter(nlab__isnull=False)
        return f'{self.environment.command} {self._get_command(params)}'

    def _get_command(self, params):
        last_params = filter(lambda x: x.type != Param.ParamChoice.ARG, params)
        last_arg = filter(lambda x: x.type == Param.ParamChoice.ARG, params)
        params_str = ' '.join([str(param) for param in last_params])
        args_str = ' '.join([str(arg) for arg in last_arg])
        return f'{params_str} {args_str}'

    def __str__(self):
        return self.name


class Environment(models.Model):
    class StateChoice:
        CREATED = 0
        BUILDING = 1
        READY = 2
        ERROR = 3

        CHOICES = (
            (CREATED, 'environment is created'),
            (BUILDING, 'environment is being built'),
            (READY, 'env is ready'),
            (ERROR, 'some error'),
        )
    created_at = models.DateTimeField(default=datetime.utcnow)
    name = models.CharField(max_length=512)
    command = models.CharField(max_length=512)
    repo_url = models.CharField(max_length=1024, null=True, blank=True)
    state = models.SmallIntegerField(choices=StateChoice.CHOICES, default=StateChoice.CREATED)
    registry_url = models.CharField(max_length=512, null=True, blank=True)
    message = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.name


class NeuroServer(models.Model):
    created_at = models.DateTimeField(default=datetime.utcnow)
    name = models.CharField(max_length=512)
    command = models.CharField(max_length=512)
    repo_url = models.CharField(max_length=1024)
    registry_url = models.CharField(max_length=512)

    def __str__(self):
        return self.name


class LearningParamValue(models.Model):
    value = models.CharField(max_length=512)
    learning = models.ForeignKey('Learning', on_delete=models.CASCADE)
    param = models.ForeignKey('Param', on_delete=models.CASCADE)

    def __str__(self):
        if self.param.type == Param.ParamChoice.FLAG:
            return str(self.param.key) if self.value == 'True' else ''
        if self.param.type == Param.ParamChoice.PARAM:
            return f'{self.param.key} {self.value}'
        if self.param.type == Param.ParamChoice.ARG:
            return str(self.value)
