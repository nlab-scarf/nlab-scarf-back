from django.apps import AppConfig


class EdbackConfig(AppConfig):
    name = 'edback'
