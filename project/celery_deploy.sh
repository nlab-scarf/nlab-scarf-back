#!/usr/bin/env bash

celery -A edback.tasks worker --loglevel=info

flower -A edback.tasks --port=5555
